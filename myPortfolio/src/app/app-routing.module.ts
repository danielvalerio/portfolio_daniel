import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('../app/pages/portfolio/portfolio.module').then((m) => m.PortfolioModule)
  },
  {
    path: 'sobre',
    loadChildren: () => import('../app/pages/about/about.module').then((m) => m.AboutModule)
  },
  {
    path: 'contato',
    loadChildren: () => import('../app/pages/contact/contact.module').then((m) => m.ContactModule)
  },
  {
    path: '**',
    loadChildren: () => import('./pages/page-not-found/page-not-found.module').then((m) => m.PageNotFoundModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
