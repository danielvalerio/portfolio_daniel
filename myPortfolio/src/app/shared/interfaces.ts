export interface Project{
    path: string
    label: string
    link?: string
}

export interface Knowledge{
    name: string
    path: string
    description: string
    link?: string
    images: string[]
}

export interface WorkExperience{
    title: string
    subtitle: string
    paragraphs: workParagraph[]
}

export interface workParagraph{
    text: string
    link?: string
}

export interface Link{
    label: string
    url: string
    icon: string
}