import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalComponent } from './modal.component';
import { MatIconModule } from '@angular/material/icon';



@NgModule({
  declarations: [
    ModalComponent
  ],
  imports: [
    CommonModule,
    MatIconModule
  ],
  exports: [
    ModalComponent
  ]
})
export class ModalModule { }
