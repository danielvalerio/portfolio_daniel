import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent {
  @Input() title = ''
  @Input() description = ''
  @Input() redirectTo? = ''
  @Input() images?: string[] = []

  @Input() isVisible = false
  @Output() isVisibleChange = new EventEmitter<boolean>()

  closeModal()
  {
    this.isVisible = false
    this.isVisibleChange.emit(false)
  }

  redirectToUrl()
  {
    if(this.redirectTo) window.location.href = this.redirectTo
  }

  slide()
  {
    const slider = document.querySelector('.content__images')! as HTMLElement
    let isDown = false;
    let startX = 0;
    let scrollLeft = 0;
    
    slider.addEventListener('mousedown', (e) => {
      isDown = true
      const ecopy = e as MouseEvent
      startX = ecopy.pageX
      scrollLeft = slider.scrollLeft
    })

    slider.addEventListener('mouseleave', () => {
      isDown = false
    })


    slider.addEventListener('mouseup', () => {
      isDown = false
    })


    slider.addEventListener('mousemove', (e) => {
      if (!isDown) return

      e.preventDefault();
      const ecopy = e as MouseEvent
      const x = ecopy.pageX - slider.offsetLeft
      const walk = (x - startX)
      slider.scrollLeft = scrollLeft - walk
    })
  }

  hasMoreThanOneImage()
  {
    if(this.images?.length! > 1) return true
    return false
  }
}
