import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HamburgerOptionsComponent } from './hamburger-options.component';



@NgModule({
  declarations: [
    HamburgerOptionsComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    HamburgerOptionsComponent
  ]
})
export class HamburgerOptionsModule { }
