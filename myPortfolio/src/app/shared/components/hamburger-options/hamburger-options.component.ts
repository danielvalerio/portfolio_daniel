import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Link } from '../../interfaces';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-hamburger-options',
  templateUrl: './hamburger-options.component.html',
  styleUrls: ['./hamburger-options.component.scss']
})
export class HamburgerOptionsComponent {
  @Input() options: Link[] = []
  @Input() isVisible = false

  @Output() isVisibleChange = new EventEmitter<boolean>();

  constructor(private readonly router: Router,)
  {

  }

  isActive(path: string): boolean {
    path = path.toLowerCase()
    const currentPath = this.router.url;
    if(currentPath === '/')
    {
      return path === '/'
    }

    console.log('path testado é ' + path + ' url testada é ' + currentPath)

    return currentPath === path ? true : false;
  }

  
  close()
  {
    this.isVisible = false
    this.isVisibleChange.emit(false)
  }

  redirectTo(url: string)
  {
    this.router.navigateByUrl(url)
  }
}
