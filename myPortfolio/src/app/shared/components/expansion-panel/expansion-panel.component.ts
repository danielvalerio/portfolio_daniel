import { Component, Input, Output, EventEmitter } from '@angular/core';
import { workParagraph } from '../../interfaces';

@Component({
  selector: 'app-expansion-panel',
  templateUrl: './expansion-panel.component.html',
  styleUrls: ['./expansion-panel.component.scss'],
})
export class ExpansionPanelComponent {
  @Input() panelIsActive = false;
  @Input() headerTitle = '';
  @Input() headerSubtitle = '';
  @Input() contentParagraphs: workParagraph[] = [];

  actualChevron = this.panelIsActive ? 'expand_less' : 'expand_more';

  togglePanel() {
    this.panelIsActive = !this.panelIsActive;
    if (this.panelIsActive) {
      this.actualChevron = 'expand_less';
    } else {
      this.actualChevron = 'expand_more';
    }
  }

  redirectToLink(link: string | undefined) {
    if (link) {
      window.location.href = link;
    }
  }
}
