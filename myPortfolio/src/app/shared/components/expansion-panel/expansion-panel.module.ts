import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExpansionPanelComponent } from './expansion-panel.component';
import { MatIconModule } from '@angular/material/icon';



@NgModule({
  declarations: [
    ExpansionPanelComponent
  ],
  imports: [
    CommonModule,
    MatIconModule
  ],
  exports: [
    ExpansionPanelComponent
  ]
})
export class ExpansionPanelModule { }
