import { Component } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent {
  birthDate = new Date(2005, 8, 12)
  photos: string[] = [
    '../../../assets/images/pessoais/eu.jpg',
    '../../../assets/images/pessoais/chapeueoculos.jpg',
    '../../../assets/images/pessoais/frenteportao.jpg',
    '../../../assets/images/pessoais/selfie1.jpg',
    '../../../assets/images/pessoais/selfie2.jpg',
    '../../../assets/images/pessoais/sentadopc.jpg',
    '../../../assets/images/pessoais/sentadopraca.jpg',
  ]

  

  setActualAge()
  {
    const today = new Date()
    const actualYear = today.getFullYear()
    const actualMonth = today.getMonth() + 1
    const actualDay = today.getDate()

    const birthYear = this.birthDate.getFullYear()
    const birthMonth = this.birthDate.getMonth() + 1
    const birthDay = this.birthDate.getDate()

    let idade = actualYear - birthYear

    if (actualMonth < birthMonth || (actualMonth === birthMonth && actualDay < birthDay))
    {
      idade--
    }

    return idade
  }
}
