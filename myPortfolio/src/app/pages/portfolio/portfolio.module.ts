import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PortfolioRoutingModule } from './portfolio-routing.module';
import { PortfolioComponent } from './portfolio.component';
import { CoreModule } from 'src/app/core/core.module';
import {MatIconModule} from '@angular/material/icon';
import { ExpansionPanelModule } from 'src/app/shared/components/expansion-panel/expansion-panel.module';
import { ModalModule } from 'src/app/shared/components/modal/modal.module';


@NgModule({
  declarations: [
    PortfolioComponent
  ],
  imports: [
    CommonModule,
    PortfolioRoutingModule,
    CoreModule,
    MatIconModule,
    ExpansionPanelModule,
    ModalModule
  ]
})
export class PortfolioModule { }
