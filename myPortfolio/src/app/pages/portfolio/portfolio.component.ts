import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Knowledge, Project, WorkExperience } from 'src/app/shared/interfaces';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss'],
})
export class PortfolioComponent {
  slider1: HTMLElement = document.getElementById(
    'Projects__slider1'
  ) as HTMLElement;
  slider2: HTMLElement = document.getElementById(
    'Projects__slider2'
  ) as HTMLElement;
  sliderContainer: HTMLElement = document.getElementById(
    'Projects'
  ) as HTMLElement;

  constructor(private readonly router: Router)
  {

  }

  ngAfterViewInit() {
    this.slider1 = document.getElementById('Projects__slider1')!;
    this.slider2 = document.getElementById('Projects__slider2')!;

    this.sliderContainer = document.getElementById('Projects')!;

    this.pauseAnimationIfWidthExceeds();
  }
  pauseAnimationIfWidthExceeds() {
    const sliderWidth = this.slider1?.scrollWidth;
    const containerWidth = this.sliderContainer?.clientWidth;

    if (sliderWidth! <= containerWidth!) {
      this.slider1.style.animation = 'none';
      this.slider2.style.display = 'none';
    }
  }

  socialLinks = [
    {
      svgPath: '../../../assets/images/instagramsvg.svg',
      label: '@_danielvalerio',
      link: 'https://www.instagram.com/_danielvalerio/'
    },
    {
      svgPath: '../../../assets/images/facebooksvg.svg',
      label: 'danielvalerio.dvc',
      link: 'https://www.facebook.com/danielvalerio.dvc'
    },
    {
      svgPath: '../../../assets/images/linkedinsvg.svg',
      label: '@daniel-valerio1',
      link: 'https://www.linkedin.com/in/daniel-valerio1/'
    },
  ]

  projects: Project[] = [
    {
      label: 'Projeto',
      path: '../../../assets/images/default-image.jpg',
    },
    {
      label: 'Projeto',
      path: '../../../assets/images/default-image.jpg',
    },
    {
      label: 'Projeto',
      path: '../../../assets/images/default-image.jpg',
    },
    {
      label: 'Projeto',
      path: '../../../assets/images/default-image.jpg',
    },
    {
      label: 'Projeto',
      path: '../../../assets/images/default-image.jpg',
    },
    {
      label: 'Projeto',
      path: '../../../assets/images/default-image.jpg',
    },
    {
      label: 'Projeto',
      path: '../../../assets/images/default-image.jpg',
    },
    {
      label: 'Projeto',
      path: '../../../assets/images/default-image.jpg',
    },
  ];

  capabilities = [
    {
      name: 'Técnico em Desenvolvimento de Sistemas (Cursando)',
      workload: 3,
      period: 'Anos',
      by: 'ETEC Dr. Demétrio Azevedo Junior',
    },
    {
      name: 'Inglês Next Generation',
      workload: 70,
      period: 'Horas',
      by: 'Wizard',
    },
    {
      name: 'Operador de Computador',
      workload: 120,
      period: 'Horas',
      by: 'NET Informática',
    },
    {
      name: 'Linguagem Java',
      workload: 40,
      period: 'Horas',
      by: 'Ginead',
    },
    {
      name: 'Funções no Excel',
      workload: 40,
      period: 'Horas',
      by: 'Ginead',
    },
    {
      name: 'Fotografia Digital',
      workload: 30,
      period: 'Horas',
      by: 'NET Informática',
    },
    {
      name: 'Estudo de Cor e Tipografia',
      workload: 10,
      period: 'Horas',
      by: 'Ginead',
    },
    {
      name: 'NextJS & React',
      workload: 6,
      period: 'Horas',
      by: 'Cod3r',
    },
    {
      name: 'Svelte',
      workload: 3,
      period: 'Horas',
      by: 'Cod3r',
    },
    
    {
      name: 'Produtividade com VSCODE',
      workload: 2,
      period: 'Horas',
      by: 'Cod3r',
    },
  ];

  tools: Knowledge[] = [
    {
      name: 'Javascript',
      path: '../../../assets/images/javascript.png',
      link: 'https://developer.mozilla.org/pt-BR/docs/Web/JavaScript',
      description: 'Linguagem de programação para web',
      images: [
        '../../../../assets/images/javascript-print.jpg',
        '../../../../assets/images/framework-javascript.jpg',
      ]
    },
    {
      name: 'Typescript',
      path: '../../../assets/images/typescript.png',
      link: 'https://www.typescriptlang.org',
      description:
        'Linguagem de programação que é a evolução do typescript, criada pela Microsoft',
        images: [
          '../../../../assets/images/js to ts.png',
          '../../../../assets/images/typescript.png',
        ]
    },
    {
      name: 'Angular',
      link: 'https://angular.io',
      path: '../../../assets/images/angular.png',
      description:
        'Um dos Frameworks de desenvolvimento web mais usados no mundo',
        images: [
          '../../../../assets/images/angular explanation.png',
          '../../../../assets/images/angular options.jpg',
        ]
    },
    {
      name: 'Figma',
      link: 'https://www.figma.com',
      path: '../../../assets/images/figma.png',
      description:
        'Ferramenta de edição de imagens e criação de protótipos de interfaces',
        images: [
          '../../../../assets/images/figma-workspace.png',
          '../../../../assets/images/figma brand.jpeg',
        ]
    },
    {
      name: 'Wix',
      link: 'https://pt.wix.com',
      path: '../../../../assets/images/wix.png',
      description:
      'Wix é uma plataforma que permite que você construa seu próprio site ou loja online sem nenhum conhecimento técnico. Trata-se de um serviço que fica na nuvem, e, com isso, quem opta por utilizá-lo não precisa se preocupar em comprar hospedagem na web ou instalar software em qualquer lugar.',
      images: [
        '../../../../assets/images/wixbg1.jpg',
        '../../../../assets/images/wixbg2.jpg'
      ]
    },
    {
      name: 'NestJS',
      link: 'https://nestjs.com',
      path: '../../../assets/images/nestjs.png',
      description:
        "Framework de criação de API's Restful com Typescript e NodeJS",
        images: [
          '../../../../assets/images/nestjs.png'
        ]
    },
    {
      name: 'Filmora',
      link: 'https://filmora.wondershare.com.br',
      path: '../../../assets/images/filmora.png',
      description: 'Editor de vídeo completo com recursos profissionais',
      images: [
        '../../../../assets/images/filmora bg.jpg',
        '../../../../assets/images/filmora timeline.png',
      ]
    },
    {
      name: 'C#',
      link: 'https://learn.microsoft.com/pt-br/dotnet/csharp/',
      path: '../../../assets/images/csharp.png',
      description:
        'Linguagem de programação para desenvolvimento de aplicações desktop criada pela Microsoft',
        images: [
          '../../../../assets/images/csharp logo.png',
          '../../../../assets/images/csharp.jpg',
        ]
    },
    {
      name: 'Git',
      link: 'https://git-scm.com',
      path: '../../../assets/images/git.png',
      description:
        'Ferramenta de versionamento de código, essencial para qualquer desenvolvedor',
        images: [
          '../../../../assets/images/git commands.jpg',
          '../../../../assets/images/git tree.png',
        ]
    },
  ];

  workExperiences: WorkExperience[] = [
    {
      title: 'Tintas Pig Itapeva Loja 02: 2023 - Atualmente',
      subtitle: 'Desenvolvedor de e-commerce; Auxiliar de vendas, estoque e entregas',
      paragraphs: [
        {
          text: "Desenvolvi inicialmente um projeto de e-commerce para as vendas locais da loja central de Itapeva-SP, utilizando da plataforma de criação de websites 'WIX', o site pode ser encontrado acessando o link:",
          link: 'https://www.tintaspig-itapeva.com.br'
        },
        {
          text: "Auxilio como vendedor na loja física, produzindo também tintas imobiliárias pigmentadas de acordo com a opção do cliente, além de auxiliar na carga e descarga no estoque de materiais e reabastecimento das prateleiras"
        }
      ]
    },
    {
      title: 'Acessórios DVC: 2022 - 2023',
      subtitle: 'Empreendimento pessoal: Loja de acessórios para celular e eletrônicos em geral, além de prestar serviços de manutenção',
      paragraphs: [
        {
          text: "Revendedor de acessórios para celular & computador na região de Itapeva-SP, tinha redes sociais como Instagram e página no Facebook para divulgação dos produtos, utilizando de banners e flyers que eu desenvolvia principalmente no Figma"
        },
        {
          text: "A divulgação era feita diariamente e os clientes podiam entrar em contato comigo através do Instagram, Messenger do Facebook, Whatsapp ou através do catálogo online que era me disponibilizado pelo aplicativo de gestão de vendas 'Trinta Shop', esta página funcionava como um expositor virtual dos produtos que eu tinha disponível a venda, semelhante a um aplicativo de Delivery, onde o cliente podia montar seu carrinho de compras e o vendedor era notificado, Este catálogo pode ser encontrado através do link:",
          link: 'https://www.trinta.shop/acessoriosdvc'
        },
        {
          text: "O armazenamento dos produtos era feito em minha própria casa, onde eu também realizava outros serviços, como formatação de celulares e montagem de computadores, além de permitir que os clientes pudessem retirar os produtos pessoalmente, caso preferissem"
        },
        {
          text: "Após a conclusão da negociação, era combinado um local de entrega ou retirada para recebimento e pagamento dos produtos"
        }
      ]
    },
  ]

  redirectToSocialMedia(
    path: string){
    window.location.href = path
  }

  actualModal = {
    title: '',
    description: '',
    redirectTo: '',
    images: ['']
  }

  modalIsOpen = false

  openModal(tool: Knowledge)
  {
    this.actualModal.description = tool.description
    this.actualModal.title = tool.name
    this.actualModal.redirectTo = tool.link!
    this.actualModal.images = tool.images
    this.modalIsOpen = true
  }
}
