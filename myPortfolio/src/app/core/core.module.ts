import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultLayoutModule } from './default-layout/default-layout.module';
import { DefaultLayoutComponent } from './default-layout/default-layout.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DefaultLayoutModule
  ],
  exports: [
    DefaultLayoutComponent
  ]
})
export class CoreModule { }
