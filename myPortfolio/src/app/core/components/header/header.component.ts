import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Link } from 'src/app/shared/interfaces';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  constructor(private readonly router: Router) {}

  actualHamburgerIcon = {
    label: 'list',
    color: 'black',
  };

  isOptionsVisible = false;
  links: Link[] = [
    {
      label: 'Meu Portfólio',
      url: '/',
      icon: 'home',
    },
    {
      label: 'Sobre',
      url: '/sobre',
      icon: 'home',
    },
    {
      label: 'Contato',
      url: '/contato',
      icon: 'home',
    },
  ];

  isFocused(path: string): boolean {
    const currentPath = this.router.url;

    if (currentPath === '/') {
      return path === '/';
    }

    return currentPath === path ? true : false;
  }

  changeIcon() {
    if (this.isOptionsVisible) {
      this.actualHamburgerIcon.label = 'close';
      this.actualHamburgerIcon.color = 'red';
    } else {
      this.actualHamburgerIcon.label = 'list';
      this.actualHamburgerIcon.color = 'black';
    }
  }

  showOptions() {
    if (this.isOptionsVisible) {
      this.isOptionsVisible = false;
      this.actualHamburgerIcon.label = 'list';
      this.actualHamburgerIcon.color = 'black';
    } else {
      this.isOptionsVisible = true;
      this.actualHamburgerIcon.label = 'close';
      this.actualHamburgerIcon.color = 'red';
    }
  }
}
