import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { HamburgerOptionsModule } from 'src/app/shared/components/hamburger-options/hamburger-options.module';

@NgModule({
  declarations: [HeaderComponent],
  imports: [CommonModule, MatMenuModule, MatIconModule, RouterModule,
  HamburgerOptionsModule],
  exports: [HeaderComponent],
})
export class HeaderModule {}
